/**
 * @file API server configuration.
 * @author <a href="http://davidrekow.com">David Rekow</a>.
 * @copyright 2015
 */

var express = require('express');

module.exports = express();
